package com.mts.teta.validation;

import com.mts.teta.model.Message;

public interface MessageValidator {

    public void validate(Message message);
    
}
