package com.mts.teta.repository;

import java.util.Optional;

import com.mts.teta.model.User;

public interface UserRepository {

    public Optional<User> findUserbyMSISDN(String msisdn);

    public void save(User user);
    
}
