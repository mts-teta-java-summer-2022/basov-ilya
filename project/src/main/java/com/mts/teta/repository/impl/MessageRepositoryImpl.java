package com.mts.teta.repository.impl;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import com.mts.teta.model.Message;
import com.mts.teta.repository.MessageRepository;

public class MessageRepositoryImpl  implements MessageRepository{

    private final List<Message> enrichedMessages = new CopyOnWriteArrayList<>();
    private final List<Message> nonEnrichedMessages = new CopyOnWriteArrayList<>();
    
    @Override
    public void saveEnrichedMessage(Message message) {
        enrichedMessages.add(new Message(message));
        
    }

    @Override
    public void saveNonEnrichedMessage(Message message) {
        nonEnrichedMessages.add(new Message(message));
        
    }

    @Override
    public List<Message> findAllEnrichedMessages() {
        return enrichedMessages.stream().map(message -> new Message(message)).collect(Collectors.toList());
    }

    @Override
    public List<Message> findAllNonEnrichedMessages() {
        return nonEnrichedMessages.stream().map(message -> new Message(message)).collect(Collectors.toList());
    }
    
}
