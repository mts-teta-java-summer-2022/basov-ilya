package com.mts.teta.repository.impl;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import com.mts.teta.model.User;
import com.mts.teta.repository.UserRepository;

public class UserRepositoryImpl implements UserRepository {

    final private List<User> users = new CopyOnWriteArrayList<>();

    @Override
    public void save(User user){
        users.add(user);
    }

    @Override
    public Optional<User> findUserbyMSISDN(String msisdn) {
        
        return users.stream().filter(user -> user.getMsisdn().equals(msisdn)).findFirst();
    }
    
}
