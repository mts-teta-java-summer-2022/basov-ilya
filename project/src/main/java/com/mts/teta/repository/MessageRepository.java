package com.mts.teta.repository;

import java.util.List;

import com.mts.teta.model.Message;

public interface MessageRepository {
    
    void saveEnrichedMessage(Message message);

    void saveNonEnrichedMessage(Message message);

    List<Message> findAllEnrichedMessages();

    List<Message> findAllNonEnrichedMessages();

}
