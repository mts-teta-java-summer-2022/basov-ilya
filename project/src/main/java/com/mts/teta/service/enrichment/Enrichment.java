package com.mts.teta.service.enrichment;

import com.mts.teta.model.Message;

public interface Enrichment {
    
    public boolean enrichMessage(Message message);
}
