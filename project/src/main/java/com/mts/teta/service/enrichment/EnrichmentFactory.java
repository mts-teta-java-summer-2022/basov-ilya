package com.mts.teta.service.enrichment;

import com.mts.teta.model.Message.EnrichmentType;
import com.mts.teta.repository.UserRepository;
import com.mts.teta.service.enrichment.impl.EnrichmentByMSISDN;

public class EnrichmentFactory {

    private final UserRepository userRepository;

    public EnrichmentFactory(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Enrichment newEnrichment(EnrichmentType type){
            
        Enrichment enrichment = null;

        switch(type) {
            case MSISDN:
                enrichment = new EnrichmentByMSISDN(userRepository);  
        }
        
        return enrichment;
    }
}
