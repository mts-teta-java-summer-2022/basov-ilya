package com.mts.teta.service.enrichment.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.json.JSONException;
import org.json.JSONObject;

import com.mts.teta.model.Message;
import com.mts.teta.model.User;
import com.mts.teta.repository.UserRepository;
import com.mts.teta.service.enrichment.Enrichment;

public class EnrichmentByMSISDN implements Enrichment{

    private final UserRepository userRepository;
    
    
    public EnrichmentByMSISDN(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public boolean enrichMessage(Message message) {

        JSONObject contentObject = new JSONObject(message.getContent());
        
        String msisdn = Optional.of(contentObject.optString("msisdn"))
                        .orElseThrow(() -> new JSONException("Field msisdn is null or is not present"));

        User user = userRepository.findUserbyMSISDN(msisdn)
                    .orElseThrow(() -> new IllegalStateException("User with msisdn: " + msisdn + " is not found"));

        Map<String, String> enrichmentMap = new HashMap<>();
        enrichmentMap.put("firstName", user.getFirstName());
        enrichmentMap.put("lastName", user.getLastName());

        contentObject.put("enrichment", new JSONObject(enrichmentMap));

        message.setContent(contentObject.toString());

        return true;

    
    }


    
    
}
