package com.mts.teta.service;

import com.mts.teta.model.Message;

public interface EnrichmentService {
    
    public String enrich(Message message);
}
