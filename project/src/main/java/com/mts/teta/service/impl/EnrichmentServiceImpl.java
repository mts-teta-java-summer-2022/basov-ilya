package com.mts.teta.service.impl;

import com.mts.teta.model.Message;
import com.mts.teta.repository.MessageRepository;
import com.mts.teta.service.EnrichmentService;
import com.mts.teta.service.enrichment.Enrichment;
import com.mts.teta.service.enrichment.EnrichmentFactory;
import com.mts.teta.validation.MessageValidator;

public class EnrichmentServiceImpl implements EnrichmentService{
   
    private final MessageValidator validator;
    private final EnrichmentFactory enrichmentFactory;
    private final MessageRepository messageRepository;

    public EnrichmentServiceImpl(MessageValidator validator, EnrichmentFactory enrichmentFactory, MessageRepository messageRepository) {
        this.validator = validator;
        this.messageRepository = messageRepository;
        this.enrichmentFactory = enrichmentFactory;
    }


    @Override
    public String enrich(Message message) {
                
        try {
            validator.validate(message);

            Enrichment enrichment = enrichmentFactory.newEnrichment(message.getEnrichmentType());

            if(enrichment.enrichMessage(message)){
                messageRepository.saveEnrichedMessage(message);
            }
        }
        catch(Exception e){
            
            System.out.println(e.toString());
            messageRepository.saveNonEnrichedMessage(message);
            
        }

        return message.getContent();
    }
}
