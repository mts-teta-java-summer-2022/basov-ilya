package com.mts.teta.model;

public class User {

    private String firstName;
    private String lastName;
    private String msisdn;
    
    public synchronized void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public synchronized void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getMsisdn() {
        return msisdn;
    }
    public synchronized void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    public String getLastName() {
        return lastName;
    }
    public User(String firstName, String lastName, String msisdn) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.msisdn = msisdn;
    }
    
}
