package com.mts.teta.model;

public class Message {
    private String content;
    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Message(Message message){
        this.content = message.getContent();
        this.enrichmentType = message.getEnrichmentType();
    }

    public Message(String content, EnrichmentType enrichmentType) {
        this.content = content;
        this.enrichmentType = enrichmentType;
    }

    private EnrichmentType enrichmentType;

    public EnrichmentType getEnrichmentType() {
        return enrichmentType;
    }

    public enum EnrichmentType {
        MSISDN;
    }
    
}