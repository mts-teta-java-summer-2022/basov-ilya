package com.mts.teta.repository;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;

import com.mts.teta.model.User;
import com.mts.teta.repository.impl.UserRepositoryImpl;

public class UserRepositoryImplTest {

    private static final UserRepository underTest = new UserRepositoryImpl();
    
    @Test
    void isShouldFindUserbyMSISDN() {

        String msisdn = "88005553535";
        User givenUser = new User("Ivan", "Ivanov", msisdn);
        underTest.save(givenUser);

        User foundUser = underTest.findUserbyMSISDN(msisdn).orElse(null);

        assertNotNull(foundUser);


    }
}
