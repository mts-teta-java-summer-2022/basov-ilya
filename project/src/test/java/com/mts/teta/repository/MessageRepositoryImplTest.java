package com.mts.teta.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.Test;

import com.mts.teta.model.Message;
import com.mts.teta.model.Message.EnrichmentType;
import com.mts.teta.repository.impl.MessageRepositoryImpl;

public class MessageRepositoryImplTest {
    
    private final MessageRepository underTest = new MessageRepositoryImpl();

    @Test
    void itShoudSaveNonEnrichedMessage() {

        String content = "{\"action\":\"button_click_03\",\"page\":\"book_card\",\"msisdn\":\"88005553535\"}";
        Message message = new Message(content,EnrichmentType.MSISDN);
        
        underTest.saveNonEnrichedMessage(message);

        List<Message> nonEnrichedMessages = underTest.findAllNonEnrichedMessages();

        assertTrue(nonEnrichedMessages.stream().allMatch(m -> Objects.equals(m.getContent(), content)));

    }

    @Test
    void itShoudSaveEnrichedMessage() {

        String content = "{\"action\":\"button_click_03\",\"page\":\"book_card\",\"msisdn\":\"88005553535\"}";
        Message message = new Message(content,EnrichmentType.MSISDN);
        
        underTest.saveEnrichedMessage(message);

        List<Message> enrichedMessages = underTest.findAllEnrichedMessages();
    
        assertTrue(enrichedMessages.stream().allMatch(m -> Objects.equals(m.getContent(), content)));

    }
}
