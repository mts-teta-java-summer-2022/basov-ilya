package com.mts.teta.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import com.mts.teta.model.Message;
import com.mts.teta.model.Message.EnrichmentType;
import com.mts.teta.model.User;
import com.mts.teta.repository.MessageRepository;
import com.mts.teta.repository.UserRepository;
import com.mts.teta.repository.impl.MessageRepositoryImpl;
import com.mts.teta.repository.impl.UserRepositoryImpl;
import com.mts.teta.service.enrichment.EnrichmentFactory;
import com.mts.teta.service.impl.EnrichmentServiceImpl;
import com.mts.teta.validation.MessageValidator;
import com.mts.teta.validation.impl.MessageValidatorImpl;

public class EnrichmentServiceTest {

    MessageValidator validator  = new MessageValidatorImpl();
    MessageRepository messageRepository = new MessageRepositoryImpl();
    UserRepository userRepository = new UserRepositoryImpl();
    EnrichmentFactory enrichmentFactory = new EnrichmentFactory(userRepository);
    final static int THREAD_POOL_SIZE = 2;
    
                                 
    @Test
    void itShouldEnrichMessage() throws JSONException, InterruptedException, ExecutionException {

        List<User> users = new ArrayList<>();
        users.add(new User("Ivan", "Ivanov",  "00000000"));
        users.add(new User("Ivan", "Sidorov",  "00000001"));
        generateUsers(users);

        ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

        EnrichmentService underTest = new EnrichmentServiceImpl(validator, enrichmentFactory, messageRepository);

        Message messageOne = new Message("{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"00000000\"}",
                                EnrichmentType.MSISDN);
        Message messageTwo = new Message("{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"00000001\"}",
                                EnrichmentType.MSISDN);


        Future<String> enrichFutureOne =  executor.submit(() -> underTest.enrich(messageOne));
        Future<String> enrichFutureTwo =  executor.submit(() -> underTest.enrich(messageTwo));

        executor.shutdown();


        //given
        String expectedFirst = "{\"enrichment\":{\"firstName\":\"Ivan\",\"lastName\":\"Ivanov\"},\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"00000000\"}";
        //when
        String actualFirst = enrichFutureOne.get();
        //then
        JSONAssert.assertEquals(expectedFirst, actualFirst, false);
             
        //given
        String expectedSecond = "{\"enrichment\":{\"firstName\":\"Ivan\",\"lastName\":\"Sidorov\"},\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"00000001\"}";
        //when
        String actualSecond  = enrichFutureTwo.get();
        //then
        JSONAssert.assertEquals(expectedSecond, actualSecond, false);

        //when
        List<Message> nonEnrichedMessages = messageRepository.findAllNonEnrichedMessages();
        //then
        assertEquals(nonEnrichedMessages.stream().anyMatch(m -> Objects.equals(m.getContent(), actualFirst))
                    && nonEnrichedMessages.stream().anyMatch(m -> Objects.equals(m.getContent(), actualSecond)), false);

        //when                    
        List<Message> enrichedMessages = messageRepository.findAllEnrichedMessages();
        //then
        assertEquals(enrichedMessages.stream().anyMatch(m -> Objects.equals(m.getContent(), actualFirst))
                    && enrichedMessages.stream().anyMatch(m -> Objects.equals(m.getContent(), actualSecond)), true);
    }


    @Test
    void itShouldNotEnrichMessage() throws JSONException, InterruptedException, ExecutionException {

        List<User> users = new ArrayList<>();
        users.add(new User("Ivan", "Koshkin",  "00000004"));
        users.add(new User("Jamal", "Sidorov",  "00000005"));
        generateUsers(users);
        

        ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        EnrichmentService underTest = new EnrichmentServiceImpl(validator, enrichmentFactory, messageRepository);

        Message messageOne = new Message("{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"00000002\"}",
                                EnrichmentType.MSISDN);
        Message messageTwo = new Message("{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"00000003\"}",
                                EnrichmentType.MSISDN);


        Future<String> enrichFutureOne =  executor.submit(() -> underTest.enrich(messageOne));
        Future<String> enrichFutureTwo =  executor.submit(() -> underTest.enrich(messageTwo));

        executor.shutdown();


        // given
        String expectedFirst = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"00000002\"}";      
        //when
        String actualFirst = enrichFutureOne.get();
        //then
        JSONAssert.assertEquals(expectedFirst, actualFirst, false);

        //given
        String expectedSecond = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"00000003\"}";
        //when
        String actualSecond = enrichFutureTwo.get();
        //then
        JSONAssert.assertEquals(expectedSecond, actualSecond, false);

        //when
        List<Message> nonEnrichedMessages = messageRepository.findAllNonEnrichedMessages();
        //then
        assertEquals(nonEnrichedMessages.stream().anyMatch(m -> Objects.equals(m.getContent(), actualFirst))
                    && nonEnrichedMessages.stream().anyMatch(m -> Objects.equals(m.getContent(), actualSecond)), true);

        //when                    
        List<Message> enrichedMessages = messageRepository.findAllEnrichedMessages();
        //then
        assertEquals(enrichedMessages.stream().anyMatch(m -> Objects.equals(m.getContent(), actualFirst))
                    && enrichedMessages.stream().anyMatch(m -> Objects.equals(m.getContent(), actualSecond)), false);

    }


    private void addTestUser(User user, CountDownLatch cdl){
        userRepository.save(user);
        cdl.countDown();
    }

    private void generateUsers(List<User> users) throws InterruptedException{

        ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        CountDownLatch cdl = new CountDownLatch(users.size());

        for (User user : users) {
            executor.submit(() -> addTestUser(user, cdl));
        }
        
        cdl.await(5000, TimeUnit.MILLISECONDS);

        executor.shutdown();

    }

}
