package com.mts.teta.validation;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.json.JSONException;
import org.junit.Test;

import com.mts.teta.model.Message;
import com.mts.teta.model.Message.EnrichmentType;
import com.mts.teta.validation.impl.MessageValidatorImpl;

public class MessageValidatorImplTest {
    private final MessageValidatorImpl underTest = new MessageValidatorImpl();

    @Test
    public void itShoudThrowsJSONExceptionForNonJsonContent(){
        Message message = new Message("Hello world", EnrichmentType.MSISDN);

        assertThrows(JSONException.class, () -> underTest.validate(message));
    }

    @Test
    public void itShoudNotThrowsJSONExceptionForJsonContent(){
        Message message = new Message("{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88005553535\"}",
                              EnrichmentType.MSISDN);

        assertDoesNotThrow(() -> underTest.validate(message));
    }
}
