package com.mts.teta.validation.impl;

import java.util.List;

import com.mts.teta.validation.TitleValidator;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TitleAnyValidator implements TitleValidator {

    private final List<TitleValidator> validators;

    @Override
    public boolean isTitleValid(String title) {

        return validators.stream().reduce(false, (v1, validator) -> {
            return v1 || validator.isTitleValid(title);
        }, (v1, v2) -> v1 || v2);
    }
}
