package com.mts.teta.validation.impl;

import java.util.regex.Pattern;

import com.mts.teta.validation.TitleAbstractValidator;
import com.mts.teta.validation.TitleValidator;

public class TitleRussianValidator extends TitleAbstractValidator implements TitleValidator {

    @Override
    public boolean isTitleValid(String title) {

        if (!Pattern.matches("(^[А-ЯЁ])([а-яё\"\\'\\,\\: ])+$", title))
            return false;

        return super.isValid(title);
    }

}
