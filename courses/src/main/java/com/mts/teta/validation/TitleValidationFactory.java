package com.mts.teta.validation;

import java.util.List;

import com.mts.teta.enums.TitleLanguage;
import com.mts.teta.validation.impl.TitleAnyValidator;
import com.mts.teta.validation.impl.TitleEnglishValidator;
import com.mts.teta.validation.impl.TitleRussianValidator;

public class TitleValidationFactory {

    public static TitleValidator createValidator(TitleLanguage language) {

        TitleValidator validator = null;

        switch (language) {
            case ANY:
                validator = new TitleAnyValidator(List.of(new TitleRussianValidator(), new TitleEnglishValidator()));
                break;
            case RU:
                validator = new TitleRussianValidator();
                break;
            case EN:
                validator = new TitleEnglishValidator();
                break;

        }
        return validator;

    }
}
