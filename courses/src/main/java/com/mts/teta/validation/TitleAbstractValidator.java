package com.mts.teta.validation;

import java.util.regex.Pattern;

public abstract class TitleAbstractValidator {

    protected boolean isValid(String title) {
        return Pattern.matches(
                "^(?! )(?!.* $)(?!(?:.*( ){2}))(?!(?:.*[~`!@#$%^&()={}\\[\\].;<>*-+?].*))(?!(?:[\r\n\t]))(\\D)+$",
                title);
        // ^(?!\\s)(?!.*\\s$)(?!(?:.*(\\s){2}))([а-яА-ЯёЁ]|[A-Za-z]|\\s|\"|\'|\\,|\\:)+$"
    }
}
