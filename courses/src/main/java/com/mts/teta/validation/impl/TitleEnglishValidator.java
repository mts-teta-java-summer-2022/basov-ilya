package com.mts.teta.validation.impl;

import java.util.regex.Pattern;

import com.mts.teta.validation.TitleAbstractValidator;
import com.mts.teta.validation.TitleValidator;

public class TitleEnglishValidator extends TitleAbstractValidator implements TitleValidator {

    @Override
    public boolean isTitleValid(String title) {

        String titleStriped = title.replaceAll(" a | for | or | not | the | and | but", " ");

        if (!Pattern.matches("^((\\b[A-Z])(\\B[a-z]*)([\"\'\\,\\: ])*)+$", titleStriped))
            return false;

        return super.isValid(title);
    }

}
