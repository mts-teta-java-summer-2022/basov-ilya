package com.mts.teta.validation;

public interface TitleValidator {

    boolean isTitleValid(String title);
}
