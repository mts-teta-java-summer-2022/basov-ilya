package com.mts.teta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mts.teta.dao.entity.Lesson;

@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long>{

}
