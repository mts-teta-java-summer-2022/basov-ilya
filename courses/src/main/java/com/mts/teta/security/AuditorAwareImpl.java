package com.mts.teta.security;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.mts.teta.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AuditorAwareImpl implements AuditorAware<Long> {

    private final UserRepository userRepository;

    @Override
    public Optional<Long> getCurrentAuditor() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null){
            return Optional.ofNullable(null);
        }        
        return userRepository.findByEmail(auth.getName()).map(user -> user.getId());
                            
    }
}