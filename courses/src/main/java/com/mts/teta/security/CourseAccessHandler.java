package com.mts.teta.security;

import java.util.NoSuchElementException;
import java.util.Objects;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.mts.teta.dao.entity.Course;
import com.mts.teta.repository.CourseRepository;
import com.mts.teta.repository.UserRepository;
import com.mts.teta.service.impl.UserAuthService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component("courseAccess")
public class CourseAccessHandler {
    private final UserRepository userRepository;
    private final UserAuthService userAuthService;
    private final CourseRepository courseRepository;
 
 
    public boolean canAssign(long userId) {

        if(!isAdmin() && !isPrivateUserAccess(userId)){
            throw new AccessDeniedException("You can not assign course to another user");
        }

        return true;
    }
 
    public boolean canUnassign(long userId) {
        if (!isAdmin() && !isPrivateUserAccess(userId)){
            throw new AccessDeniedException("You can not unassign course from another user");
        }

        return true;
    }

    public boolean canUpdate(long courseId){
        if (!(isAdmin() || isPrivateCourseAccess(courseId) && isTeacher())){
            throw new AccessDeniedException("Course can be created/updated only by Administrator or Teacher");
        }

        return true; 
        
    }

    public boolean canDelete(long courseId){

        if (!isAdmin() && !isPrivateCourseAccess(courseId)){
            throw new AccessDeniedException("Course can be deleted only by Administrator or Author");
        }

        return true;

    }
 
    private boolean isPrivateUserAccess(long userId) {
        String userName = userAuthService.getAuthenticatedUser().getUsername();
        return Objects.equals(userRepository.findByEmail(userName).get().getId(), userId);
    }

    private boolean isPrivateCourseAccess(long courseId) {
        String userName = userAuthService.getAuthenticatedUser().getUsername();
        Course course = courseRepository.findById(courseId)
                .orElseThrow(() -> new NoSuchElementException("Course with id: " + courseId + " is not found"));

        return Objects.equals(userRepository.findByEmail(userName).get().getId(), course.getCreatedBy());
    }

    private boolean isAdmin(){
        return userAuthService.getAuthenticatedUser().getAuthorities()
        .stream().filter(c -> Objects.equals(c.getAuthority(), "ROLE_ADMIN") || Objects.equals(c.getAuthority(), "ROLE_OWNER")).count() > 0;
    }

    private boolean isTeacher(){
        return userAuthService.getAuthenticatedUser().getAuthorities()
        .stream().filter(c -> Objects.equals(c.getAuthority(), "ROLE_TEACHER")).count() > 0;
    }
}
