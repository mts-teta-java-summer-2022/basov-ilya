package com.mts.teta.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.mts.teta.validation.TitleValidationFactory;
import com.mts.teta.validation.TitleValidator;

public class TitleCaseValidator implements ConstraintValidator<TitleCase, String> {
    private TitleValidator validator;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        return validator.isTitleValid(value);
    }

    @Override
    public void initialize(TitleCase constraintAnnotation) {
        this.validator = TitleValidationFactory.createValidator(constraintAnnotation.language());

    }
}