package com.mts.teta.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.mts.teta.enums.TitleLanguage;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TitleCaseValidator.class)
public @interface TitleCase {

       public TitleLanguage language() default TitleLanguage.ANY;

       public String message() default "Неверный формат заголовка";

       public Class<?>[] groups() default {};

       public Class<? extends Payload>[] payload() default {};

}
