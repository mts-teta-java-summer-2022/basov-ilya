package com.mts.teta.util;

import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;

public class SelfValidated {
    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    public void validateSelf() {
        final var constraintViolations = VALIDATOR.validate(this);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
    }
}
