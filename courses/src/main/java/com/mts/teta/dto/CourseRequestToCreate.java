package com.mts.teta.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseRequestToCreate {

    private String author;
    @NotEmpty(message = "title can not be empty")
    @NotNull(message = "title can not be null")
    private String title;
    @NotEmpty(message = "description can not be empty")
    @NotNull(message = "description can not be null")
    private String description;
    
}
