package com.mts.teta.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CoursePagebleResponse {
    private List<CourseResponse> content;
    private int page;
    private int pageSize;
    private long totalElements;
    private int totalPages;
    private boolean last;

    
}
