package com.mts.teta.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRequest {
    @NotNull(message = "Email can not be null")
    @NotBlank(message = "Email can not be empty")
    private String email;

   @NotNull(message = "Password can not be null")
   @NotBlank(message = "Password can not be empty")
   private String password;


}
