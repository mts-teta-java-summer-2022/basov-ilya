package com.mts.teta.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CourseResponse {
    
    private Long id;
    private String title;
    private String description;
    private List<String> users;
}
