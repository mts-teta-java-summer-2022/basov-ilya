package com.mts.teta.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LessonResponse {
    private Long id;
    private String title;
    private String content;
}
