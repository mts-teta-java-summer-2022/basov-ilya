package com.mts.teta.dao.entity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@EntityListeners({AuditingEntityListener.class})
@Table(name = "course", schema = "public")
public class Course {
    @Id
    // @SequenceGenerator(
    //         name = "course_sequence",
    //         sequenceName = "course_sequence",
    //         allocationSize = 1
    // )
    @GeneratedValue(
            strategy = GenerationType.IDENTITY

    )
    @Column(name = "id")
    private Long id;
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "description", nullable = false)
    private String description;
    @Column(name = "author")
    private String author;
    @OneToMany(mappedBy = "course", orphanRemoval = true,  cascade = CascadeType.ALL)
    private List<Lesson> lessons;
    @ManyToMany
    @JoinTable(
        name = "user_course", 
        joinColumns = { @JoinColumn(name = "course_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    private Set<User> users;

    @Column(name = "created_dt")
    @CreatedDate
    private LocalDateTime createdDate;

    @Column(name = "last_modified_dt")
    @LastModifiedDate
    private LocalDateTime modifiedDate;

    @Column(name = "created_by")
    @CreatedBy
    private Long createdBy;
  
    @Column(name = "last_modified_by")
    @LastModifiedBy
    private Long modifiedBy;

    
    public Course(String title, String description, String author) {
        this.title = title;
        this.description = description;
        this.author = author;
    }

}
