package com.mts.teta.dao.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EntityListeners({AuditingEntityListener.class})
@Table(name = "user", schema = "public")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String email;

    @Column
    private String password;
    
    @ManyToMany(mappedBy = "users")
    private Set<Course> courses;

    @ManyToMany(mappedBy = "users")
    private Set<Role> roles;

    public User(String email) {
        this.email = email;
    }

    @Column(name = "created_dt")
    @CreatedDate
    private LocalDateTime createdDate;

    @Column(name = "last_modified_dt")
    @LastModifiedDate
    private LocalDateTime modifiedDate;

    @Column(name = "created_by")
    @CreatedBy
    private Long createdBy;
  
    @Column(name = "last_modified_by")
    @LastModifiedBy
    private Long modifiedBy;

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}