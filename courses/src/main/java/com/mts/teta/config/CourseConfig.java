package com.mts.teta.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;
import lombok.NoArgsConstructor;

@Configuration
@ConfigurationProperties(prefix = "course")
@PropertySource("classpath:courseprops.properties")
@Data
@NoArgsConstructor
public class CourseConfig {
    private int defaultPageNumber;
    private int defaultPageSize;
    private String defaultSortBy;
    private String defaultSortOrder;
}
