package com.mts.teta.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.mts.teta.repository.UserRepository;
import com.mts.teta.security.AuditorAwareImpl;

@Configuration
@EnableJpaAuditing
public class AuditConfig {
    @Bean
    AuditorAware<Long> auditorProvider(@Autowired UserRepository userRepository) {
        return new AuditorAwareImpl(userRepository);
    }
    
}
