package com.mts.teta.enums;

public enum TitleLanguage {
    RU,
    EN,
    ANY
}
