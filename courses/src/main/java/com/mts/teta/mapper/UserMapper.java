package com.mts.teta.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mts.teta.dao.entity.User;
import com.mts.teta.dto.UserRequest;
import com.mts.teta.dto.UserResponse;

@Mapper (componentModel = "spring", uses = PasswordEncoderMapper.class)
public interface UserMapper {

    @Mapping(source = "password", target = "password", qualifiedByName ="encodedMapping")
    User mapUserRequestDtoToEntity(UserRequest source);

    UserResponse mapEntityToUserResponseDto(User source);
    
}
