package com.mts.teta.mapper;

import org.mapstruct.Mapper;

import com.mts.teta.dao.entity.Lesson;
import com.mts.teta.dto.LessonRequest;
import com.mts.teta.dto.LessonResponse;

@Mapper (componentModel = "spring")
public interface LessonMapper {
    Lesson mapLessonRequestDtoToEntity(LessonRequest source);
    LessonResponse mapEntityToLessonResponseDto(Lesson source);
    
}

