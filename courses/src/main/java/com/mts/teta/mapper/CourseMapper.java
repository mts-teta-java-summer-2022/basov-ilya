package com.mts.teta.mapper;

import org.mapstruct.Mapper;

import com.mts.teta.dao.entity.Course;
import com.mts.teta.dao.entity.User;
import com.mts.teta.dto.CourseRequestToCreate;
import com.mts.teta.dto.CourseRequestToUpdate;
import com.mts.teta.dto.CourseResponse;

@Mapper (componentModel = "spring")
public interface CourseMapper {
    Course mapCreateDtoToEntity(CourseRequestToCreate source);
    Course mapUpdateDtoToEntity(CourseRequestToUpdate source);
    CourseResponse mapToResponseDto(Course source);

    default String mapToUsersString(User user) {
        return user.getEmail();
      }
    
}
