package com.mts.teta.service;

import org.springframework.data.domain.Page;

import com.mts.teta.dao.entity.Course;
import com.mts.teta.dao.entity.Lesson;

public interface CourseService {
    
    public Page<Course> getCourses(int page, int pageSize, String sortBy, String sortDir);
    public Page<Course> getCoursesByTitleWithPrefix(String prefix, int page, int pageSize, String sortBy, String sortDir);
    public Course getCourse(Long id);
    public void deleteCourse(Long id);
    public Course createCourse(Course course);
    public void updateCourse(Long id, Course course);
    public Lesson assignLessonToCourse(Long id, Lesson lesson);
    public void updateLessonAssignedToCourse(Long courseId, Long lessonId, Lesson lesson);
    public void deleteLessonAssignedToCourse(Long courseId, Long lessonId);
    public void assignUserToCourse(Long courseId, Long userId);
    public void unassignUserFromCourse(Long courseId, Long userId);

}
