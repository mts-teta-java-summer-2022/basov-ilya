package com.mts.teta.service.impl;

import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.mts.teta.dao.entity.Role;
import com.mts.teta.dao.entity.User;
import com.mts.teta.repository.RoleRepository;
import com.mts.teta.repository.UserRepository;
import com.mts.teta.service.UserService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
    
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    
    @Override
    @Transactional
    public User createUser(User user) {
        String email = user.getEmail();
        boolean exists = !userRepository.findByEmail(email).isEmpty();
        if(exists){
            throw new IllegalArgumentException("User with : " + email + " already exists");
        }
        
        return userRepository.save(user);

    }

    @Override
    @Transactional
    public void updateUser(Long id, User user) {
       
        userRepository.findById(id).orElseThrow( () -> new NoSuchElementException("User with id: " + id + " is not found"));
        user.setId(id);
        userRepository.save(user);
        
    }
    
    @Override
    @Transactional
    public void deleteUser(Long id){
        
        userRepository.findById(id).orElseThrow( () -> new NoSuchElementException("User with id: " + id + " is not found"));
        
        userRepository.deleteById(id);

    }

    @Override
    @Transactional
    public void assignRoleToUser(Long userId, Long roleId) {
        User user = userRepository.findById(userId)
                        .orElseThrow(() -> new NoSuchElementException("User with id: " + userId + " is not found")); 

        Role role = roleRepository.findById(roleId)
                .orElseThrow(() -> new NoSuchElementException("Role with id: " + roleId + " is not found"));

        role.getUsers().add(user);
        user.getRoles().add(role);
        userRepository.save(user);  
    }

    @Override
    @Transactional
    public void unassignRoleFromUser(Long userId, Long roleId) {
        User user = userRepository.findById(userId)
        .orElseThrow(() -> new NoSuchElementException("User with id: " + userId + " is not found")); 

        Role role = roleRepository.findById(roleId)
        .orElseThrow(() -> new NoSuchElementException("Role with id: " + roleId + " is not found"));

        role.getUsers().remove(user);
        user.getRoles().remove(role);
        userRepository.save(user);  
        
    }
    
    
}
