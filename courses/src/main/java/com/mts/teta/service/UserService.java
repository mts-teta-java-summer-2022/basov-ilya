package com.mts.teta.service;

import com.mts.teta.dao.entity.User;

public interface UserService {
    public User createUser(User user);
    public void updateUser(Long id, User user);
    public void deleteUser(Long id);
    public void assignRoleToUser(Long userId, Long roleId);
    public void unassignRoleFromUser(Long userId, Long roleId);
    
}
