package com.mts.teta.service.impl;

import java.util.NoSuchElementException;
import java.util.Objects;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mts.teta.dao.entity.Course;
import com.mts.teta.dao.entity.Lesson;
import com.mts.teta.dao.entity.User;
import com.mts.teta.repository.CourseRepository;
import com.mts.teta.repository.LessonRepository;
import com.mts.teta.repository.UserRepository;
import com.mts.teta.service.CourseService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;
    private final LessonRepository lessonRepository;
    private final UserRepository userRepository;

    @Override
    public Page<Course> getCourses(int page, int pageSize, String sortBy, String sortDir){

        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(page, pageSize, sort);

        return courseRepository.findAll(pageable);
    }

    @Override
    public Page<Course> getCoursesByTitleWithPrefix(String prefix, int page, int pageSize, String sortBy, String sortDir){
        
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(page, pageSize, sort);
        return Objects.requireNonNullElse(prefix, "").isEmpty() ? courseRepository.findAll(pageable) : courseRepository.findByTitleWithPrefix(prefix, pageable);
    }

    @Override
    public Course getCourse(Long id){
        return courseRepository.findById(id)
            .orElseThrow(() -> new NoSuchElementException("Course with id: " + id + " is not found"));
    }

    @Override
    @Transactional
    public void deleteCourse(Long id){       
        courseRepository.findById(id)
            .orElseThrow(() -> new NoSuchElementException("Course with id: " + id + " is not found"));

        courseRepository.deleteById(id);

    }

    @Override
    @Transactional
    public Course createCourse(Course course){
        courseRepository.save(course);
        return course;
    }

    @Override
    @Transactional
    public void updateCourse(Long id, Course course){
        courseRepository.findById(id)
            .orElseThrow(() -> new NoSuchElementException("Course with id: " + id + " is not found"));

        course.setId(id);
        courseRepository.save(course);
    }

    @Override
    public Lesson assignLessonToCourse(Long id, Lesson lesson) {
        Course course = courseRepository.getById(id);
        lesson.setCourse(course);

        return lessonRepository.save(lesson);
        
    }

    @Override
    public void updateLessonAssignedToCourse(Long courseId, Long lessonId, Lesson lesson) {
        Course course = courseRepository.getById(courseId);
        lesson.setCourse(course);

        lessonRepository.findById(lessonId)
                            .orElseThrow(() -> new NoSuchElementException("Lesson with id: " + lessonId + " is not found"));

        lesson.setId(lessonId);
        lessonRepository.save(lesson);
        
    }

    @Override
    public void deleteLessonAssignedToCourse(Long courseId, Long lessonId) {
        courseRepository.findById(lessonId)
            .orElseThrow(() -> new NoSuchElementException("Lesson with id: " + lessonId + " is not found"));

        lessonRepository.deleteById(lessonId);
        
    }

    @Override
    public void assignUserToCourse(Long courseId, Long userId) {
        User user = userRepository.findById(userId)
                        .orElseThrow(() -> new NoSuchElementException("User with id: " + userId + " is not found")); 

        Course course = courseRepository.findById(courseId)
                        .orElseThrow(() -> new NoSuchElementException("Course with id: " + courseId + " is not found"));

        course.getUsers().add(user);
        user.getCourses().add(course);
        courseRepository.save(course);       
    }

    @Override
    public void unassignUserFromCourse(Long courseId, Long userId) {
        User user = userRepository.findById(userId)
                        .orElseThrow(() -> new NoSuchElementException("User with id: " + userId + " is not found")); 
                               
        Course course = courseRepository.findById(courseId)
                        .orElseThrow(() -> new NoSuchElementException("Course with id: " + courseId + " is not found"));

        user.getCourses().remove(course);
        course.getUsers().remove(user);
        courseRepository.save(course);
    }
    
}
