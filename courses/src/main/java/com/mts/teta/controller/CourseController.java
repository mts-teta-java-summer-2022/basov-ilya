package com.mts.teta.controller;

import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mts.teta.config.CourseConfig;
import com.mts.teta.dao.entity.Course;
import com.mts.teta.dto.CoursePagebleResponse;
import com.mts.teta.dto.CourseRequestToCreate;
import com.mts.teta.dto.CourseRequestToUpdate;
import com.mts.teta.dto.CourseResponse;
import com.mts.teta.dto.LessonRequest;
import com.mts.teta.dto.LessonResponse;
import com.mts.teta.mapper.CourseMapper;
import com.mts.teta.mapper.LessonMapper;
import com.mts.teta.service.CourseService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "api/v1/course")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;
    private final CourseMapper courseMapper;
    private final CourseConfig courseConfig;
    private final LessonMapper lessonMapper;

    @GetMapping("/{id}")
    public CourseResponse getCourse(@PathVariable("id") Long id) {
        return courseMapper.mapToResponseDto(courseService.getCourse(id));
    }

    @GetMapping
    public CoursePagebleResponse getAllCourses(@RequestParam(value = "page", required = false) Integer page,
                                                 @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                                 @RequestParam(value = "sortBy", required = false) String sortBy,
                                                 @RequestParam(value = "sortDir", required = false) String sortDir){ 

        Page<Course> courses = courseService.getCourses(Objects.requireNonNullElse(page, courseConfig.getDefaultPageNumber()),
                                                        Objects.requireNonNullElse(pageSize, courseConfig.getDefaultPageSize()),
                                                        Objects.requireNonNullElse(sortBy, courseConfig.getDefaultSortBy()),
                                                        Objects.requireNonNullElse(sortDir, courseConfig.getDefaultSortOrder()));

        return new CoursePagebleResponse(courses.getContent().stream().map(courseMapper::mapToResponseDto).collect(Collectors.toList()),
                                        courses.getNumber(),
                                        courses.getSize(),
                                        courses.getTotalElements(),
                                        courses.getTotalPages(),                                       
                                        courses.isLast());
    }

    @GetMapping("/filter")
    public CoursePagebleResponse getCoursesByTitlePrefix(@RequestParam(name = "titlePrefix", required = false) String titlePrefix,
                                                        @RequestParam(value = "page", required = false) Integer page,
                                                        @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                                        @RequestParam(value = "sortBy", required = false) String sortBy,
                                                        @RequestParam(value = "sortDir", required = false) String sortDir) {
        
        Page<Course> courses = courseService.getCoursesByTitleWithPrefix(titlePrefix, 
                                                                         Objects.requireNonNullElse(page, courseConfig.getDefaultPageNumber()),
                                                                         Objects.requireNonNullElse(pageSize, courseConfig.getDefaultPageSize()),
                                                                         Objects.requireNonNullElse(sortBy, courseConfig.getDefaultSortBy()),
                                                                         Objects.requireNonNullElse(sortDir, courseConfig.getDefaultSortOrder()));
        
        return new CoursePagebleResponse(courses.getContent().stream().map(courseMapper::mapToResponseDto).collect(Collectors.toList()),
                courses.getNumber(),
                courses.getSize(),
                courses.getTotalElements(),
                courses.getTotalPages(),                                       
                courses.isLast());     
                                                            
    }
    @PreAuthorize("hasAnyAuthority('ROLE_OWNER','ROLE_ADMIN','ROLE_TEACHER')")
    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping
    public CourseResponse createCourse(@Valid @RequestBody CourseRequestToCreate request) {
        return courseMapper.mapToResponseDto(courseService.createCourse(courseMapper.mapCreateDtoToEntity(request)));
    }

    @PutMapping("/{id}")
    @PreAuthorize("@courseAccess.canUpdate(#courseId)")
    public void updateCourse(@PathVariable Long id, 
                             @RequestBody @Valid CourseRequestToUpdate request) {
        courseService.updateCourse(id, courseMapper.mapUpdateDtoToEntity(request));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("@courseAccess.canDelete(#id)")
    public void deleteCourse(@PathVariable("id") Long id) {
        courseService.deleteCourse(id);
    }

    @PostMapping("/{id}/lessons")
    @PreAuthorize("@courseAccess.canUpdate(#id)")
    public LessonResponse assignLessonToCourse(@PathVariable Long id,
                             @RequestBody @Valid LessonRequest request) {
        return lessonMapper.mapEntityToLessonResponseDto(courseService.assignLessonToCourse(id, lessonMapper.mapLessonRequestDtoToEntity(request)));
    }

    @PutMapping("/{courseId}/lessons/{lessonId}")
    @PreAuthorize("@courseAccess.canUpdate(#courseId)")
    public void updateLessonAssignedToCourse(@PathVariable Long courseId, @PathVariable Long lessonId, 
                             @RequestBody @Valid LessonRequest request) {
        courseService.updateLessonAssignedToCourse(courseId, lessonId, lessonMapper.mapLessonRequestDtoToEntity(request));
    }

    @DeleteMapping("/{courseId}/lessons/{lessonId}")
    @PreAuthorize("@courseAccess.canUpdate(#courseId)")
    public void deleteLessonAssignedToCourse(@PathVariable Long courseId, @PathVariable Long lessonId) {
        courseService.deleteLessonAssignedToCourse(courseId, lessonId);
    }

    @PutMapping("{courseId}/user/{userId}")
    @PreAuthorize("@courseAccess.canAssign(#userId)")
    public void assignUserToCourse(@PathVariable Long courseId, @PathVariable Long userId){
        courseService.assignUserToCourse(courseId, userId);
    }

    @DeleteMapping("{courseId}/user/{userId}")
    @PreAuthorize("@courseAccess.canUnassign(#userId)")
    public void unassignUserFromCourse(@PathVariable Long courseId, @PathVariable Long userId){
        courseService.unassignUserFromCourse(courseId, userId);
    }


    
}
