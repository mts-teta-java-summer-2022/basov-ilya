package com.mts.teta.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mts.teta.dto.UserRequest;
import com.mts.teta.dto.UserResponse;
import com.mts.teta.mapper.UserMapper;
import com.mts.teta.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "api/v1/admin/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;
    

    @PostMapping
    public UserResponse createUser(@Valid @RequestBody UserRequest request) {
        return userMapper.mapEntityToUserResponseDto(
            userService.createUser(userMapper.mapUserRequestDtoToEntity(request))
        );
    }

    @PutMapping("/{id}")
    public void updateUser(@PathVariable Long id, 
                             @RequestBody @Valid UserRequest request) {
        userService.updateUser(id, userMapper.mapUserRequestDtoToEntity(request));
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.deleteUser(id);
    }

    @PutMapping("/{userId}/role/{roleId}")
    public void assignRoleToUser(@PathVariable Long userId, @PathVariable Long roleId) {
       userService.assignRoleToUser(userId, roleId);
    }

    @DeleteMapping("/{userId}/role/{roleId}")
    public void unassignRoleToUser(@PathVariable Long userId, @PathVariable Long roleId) {
        userService.unassignRoleFromUser(userId, roleId);
     }
}
