select p.post_id 
from post p inner join comment c on p.post_id = c.post_id 
where p.title ~ '^[0-9]' and length(p.content) > 20 
group by p.post_id having count(c.comment_id) = 2;

-- output
-- 22
-- 24
-- 26
-- 28
-- 32
-- 34
-- 36
-- 38
-- 42
-- 44