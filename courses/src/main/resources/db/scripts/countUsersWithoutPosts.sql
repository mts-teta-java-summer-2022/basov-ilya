select count(*) cnt
from profile p 
where not exists (
        select 1 
        from post pp 
        where pp.profile_id = p.profile_id
        );
-- output:
--     5