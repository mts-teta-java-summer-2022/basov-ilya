select tt.post_id 
from ( select p.post_id, rank() over (order by p.post_id asc) rn
       from post p left join comment c on p.post_id = c.post_id
       group by p.post_id having count(c.comment_id) <= 1
     ) tt
where tt.rn <= 3;

-- output
-- 1
-- 3
-- 5