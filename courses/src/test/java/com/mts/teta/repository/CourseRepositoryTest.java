package com.mts.teta.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;

import com.mts.teta.dao.entity.Course;

@DataJpaTest
@AutoConfigureTestDatabase
@TestPropertySource(
        locations = "classpath:application-h2.properties"
)
public class CourseRepositoryTest {
    @Autowired
    private CourseRepository underTest;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void itShouldFindCourseByPrefix(){

        Course courseOne = new Course("Java Generics", "Youtube video", "Amigoscode");
        underTest.save(courseOne);

        Course courseTwo = new Course("Java Steams API", "Youtube video", "Amigoscode");
        underTest.save(courseTwo);

        Course courseThree = new Course("Software Testing", "Youtube video", "Amigoscode");
        underTest.save(courseThree);

        Pageable pageable = PageRequest.of(0, 10, Sort.by("id").ascending());
        assertEquals(2, underTest.findByTitleWithPrefix("Java", pageable).getContent().size());




    }
}
