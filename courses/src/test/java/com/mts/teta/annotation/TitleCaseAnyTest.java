package com.mts.teta.annotation;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Test;

import com.mts.teta.util.SelfValidated;

public class TitleCaseAnyTest {
    private class CourseAny extends SelfValidated {
        @TitleCase
        private final String title;

        CourseAny(String title) {
            this.title = title;
            validateSelf();
        }
    }

    @Test
    void itShouldThrowsWhenTitleContainsSpecialCharacter() {

        assertThrows(ConstraintViolationException.class, () -> new CourseAny("I love $$$"));

    }

    @Test
    void itShouldnotThrowsWhenEnglishTitleIsCorrect() {

        assertDoesNotThrow(() -> new CourseAny("Java Course for \'Dummies\'"));

    }

    @Test
    void itShouldnotThrowsWhenRussianTitleIsCorrect() {

        assertDoesNotThrow(() -> new CourseAny("Как заработать \'зеленых\'"));

    }

    @Test
    void itShouldThrowsWhenEnglishTitleCaseIsIncorrect() {

        assertThrows(ConstraintViolationException.class, () -> new CourseAny("Java course for \'dummies\'"));

    }

    @Test
    void itShouldThrowsWhenRussianTitleCaseIsIncorrect() {

        assertThrows(ConstraintViolationException.class, () -> new CourseAny("Как заработать Миллион"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsMixedCharacters() {

        assertThrows(ConstraintViolationException.class, () -> new CourseAny("Java курс"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsTwoSpaces() {

        assertThrows(ConstraintViolationException.class, () -> new CourseAny("Java  Course for \'Dummies\'"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsTabSpaces() {

        assertThrows(ConstraintViolationException.class, () -> new CourseAny("Java \t Course for \'dummies\'"));

    }
}
