package com.mts.teta.annotation;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Test;

import com.mts.teta.enums.TitleLanguage;
import com.mts.teta.util.SelfValidated;

public class TitleCaseRuTest {

    private class CourseRu extends SelfValidated {
        @TitleCase(language = TitleLanguage.RU)
        private final String title;

        CourseRu(String title) {
            this.title = title;
            validateSelf();
        }
    }

    @Test
    void itShouldThrowsWhenTitleContainsEnCharacter() {

        assertThrows(ConstraintViolationException.class, () -> new CourseRu("Java course"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsSpecialCharacter() {

        assertThrows(ConstraintViolationException.class, () -> new CourseRu("Как заработать $$$"));

    }

    @Test
    void itShouldnotThrowsWhenTitleIsCorrect() {

        assertDoesNotThrow(() -> new CourseRu("Как заработать \'зеленые\'"));

    }

    @Test
    void itShouldThrowsWhenTitleCaseIsIncorrect() {

        assertThrows(ConstraintViolationException.class, () -> new CourseRu("Как заработать доллар США"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsMixedCharacters() {

        assertThrows(ConstraintViolationException.class, () -> new CourseRu("Java курс"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsTwoSpaces() {

        assertThrows(ConstraintViolationException.class, () -> new CourseRu("Как  заработать \'зеленые\'"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsTabSpaces() {

        assertThrows(ConstraintViolationException.class, () -> new CourseRu("Как \t заработать \'зеленые\'"));

    }

}
