package com.mts.teta.annotation;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Test;

import com.mts.teta.enums.TitleLanguage;
import com.mts.teta.util.SelfValidated;

public class TitleCaseEnTest {
    private class CourseEn extends SelfValidated {
        @TitleCase(language = TitleLanguage.EN)
        private final String title;

        CourseEn(String title) {
            this.title = title;
            validateSelf();
        }
    }

    @Test
    void itShouldThrowsWhenTitleContainsRuCharacter() {

        assertThrows(ConstraintViolationException.class, () -> new CourseEn("Курс программирования"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsSpecialCharacter() {

        assertThrows(ConstraintViolationException.class, () -> new CourseEn("I love $$$"));

    }

    @Test
    void itShouldnotThrowsWhenTitleIsCorrect() {

        assertDoesNotThrow(() -> new CourseEn("Java Course for \'Dummies\'"));

    }

    @Test
    void itShouldThrowsWhenTitleCaseIsIncorrect() {

        assertThrows(ConstraintViolationException.class, () -> new CourseEn("Java course for \'dummies\'"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsMixedCharacters() {

        assertThrows(ConstraintViolationException.class, () -> new CourseEn("Java курс"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsTwoSpaces() {

        assertThrows(ConstraintViolationException.class, () -> new CourseEn("Java  Course for \'Dummies\'"));

    }

    @Test
    void itShouldThrowsWhenTitleContainsTabSpaces() {

        assertThrows(ConstraintViolationException.class, () -> new CourseEn("Java \t Course for \'dummies\'"));

    }
}
