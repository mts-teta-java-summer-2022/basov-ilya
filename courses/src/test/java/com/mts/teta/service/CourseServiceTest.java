package com.mts.teta.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mts.teta.dao.entity.Course;
import com.mts.teta.repository.CourseRepository;
import com.mts.teta.service.impl.CourseServiceImpl;

@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ExtendWith(MockitoExtension.class)
public class CourseServiceTest {

    @Mock private CourseRepository courseRepository;
    private CourseServiceImpl underTest;

    @BeforeEach
    void setUp() {
        underTest = new CourseServiceImpl(courseRepository, null, null);
    }


    @Test
    void canGetAllCourses() {
        
        Pageable pageable = PageRequest.of(0, 10, Sort.by("id").ascending());
        
        underTest.getCourses(0, 10, "id", "asc");
        verify(courseRepository).findAll(pageable);
    }

    @Test
    void canGetCoursesByTitleWithPrefix() {   
        String prefix = "test";

        Pageable pageable = PageRequest.of(0, 10, Sort.by("id").ascending());

        underTest.getCoursesByTitleWithPrefix(prefix, 0, 10, "id","asc");
        verify(courseRepository).findByTitleWithPrefix(prefix, pageable);
    }

    @Test
    void canCreateCourse(){

        Course course = new Course("Software testing", "Youtube video","Amigoscode");
        underTest.createCourse(course);

        ArgumentCaptor<Course> courseArgumentCaptor =
                ArgumentCaptor.forClass(Course.class);

        verify(courseRepository)
                .save(courseArgumentCaptor.capture());

        Course capturedCourse = courseArgumentCaptor.getValue();

        assertThat(capturedCourse).isEqualTo(course);

    }

    @Test
    void canDeleteCourse() {

        long id = 42;
        given(courseRepository.findById(id))
                .willReturn(Optional.of(new Course()));

        underTest.deleteCourse(id);
        verify(courseRepository).deleteById(id);
    }

    @Test
    void canUpdateCourse() {

        long id = 42;
        Course course = new Course("Software testing", "Youtube video","Amigoscode");

        given(courseRepository.findById(id))
                .willReturn(Optional.of(new Course()));

        underTest.updateCourse(id, course);


        ArgumentCaptor<Course> courseArgumentCaptor =
                ArgumentCaptor.forClass(Course.class);

         verify(courseRepository).save(courseArgumentCaptor.capture());

        Course capturedCourse = courseArgumentCaptor.getValue();

        assertThat(capturedCourse).isEqualTo(course);

    }

    @Test
    void willThrowWhenDeleteCourseNotFound() {
  
        long id = 1;

        assertThatThrownBy(() -> underTest.deleteCourse(id))
                .isInstanceOf(NoSuchElementException.class)
                .hasMessageContaining("Course with id: " + id + " is not found");

        verify(courseRepository, never()).deleteById(any());
    }

    @Test
    void willThrowWhenUpdateCourseNotFound() {
  
        long id = 42;
        Course course = new Course("Software testing", "Youtube video","Amigoscode");
                
        assertThatThrownBy(() -> underTest.updateCourse(id, course))
                .isInstanceOf(NoSuchElementException.class)
                .hasMessageContaining("Course with id: " + id + " is not found");

        verify(courseRepository, never()).save(any());
    }


}
