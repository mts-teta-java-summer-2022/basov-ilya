package com.mts.teta.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.mts.teta.dto.CourseRequestToCreate;
import com.mts.teta.dto.CourseResponse;
import com.mts.teta.repository.CourseRepository;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class CourseControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Autowired
    private CourseRepository courseRepository;

   
    @WithMockUser(username="teacher@company.com", password="teacher", roles={"TEACHER"})
    @Test
    void teacherCanCreateNewCourse() throws Exception{


        CourseRequestToCreate courseDtoRequest = new CourseRequestToCreate("Anvar",
                                        "How to work from Istanbul", "Remote java course" );
        ResultActions resultActions = mockMvc
                                        .perform(post("/api/v1/course")
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .content(objectMapper.writeValueAsString(courseDtoRequest))
                                       );
        // then
        MvcResult postCourseResult  = resultActions.andExpect(status().isCreated()).andReturn();


        String contentAsString = postCourseResult.getResponse().getContentAsString();

        CourseResponse courseDtoResponse = objectMapper.readValue(
                contentAsString, new TypeReference<>() {

                }
        );

         boolean exists = courseRepository.existsById(courseDtoResponse.getId());

         assertThat(exists).isTrue();

    }

    @WithMockUser(username="student@company.com", password="student", roles={"STUDENT"})
    @Test
    void studentCanNotCreateNewCourse() throws Exception{

        CourseRequestToCreate courseDtoRequest = new CourseRequestToCreate("Anvar",
                                                            "How to work from Istanbul", "Remote java course" );
        mockMvc
                .perform(post("/api/v1/course")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(courseDtoRequest))
            ).andExpect(status().isForbidden());
        
    }


    
}
